# epidemic_model

Project: #epidemic-model

1. Mission and goals of the work stream

Research, build and deploy epidemic models to allow users to understand the future impact of COVID-19.

Our mission is twofold: 1) use models to minimize the number of deaths and burden on society, and 2) disseminate "interesting" information to the public/media.

The primary focus has to be delivering an application to users. Models can be improved over time, by releasing new features. If we do not present and popularize our work then we are wasting time.

2. Important Links

Project Page: TBC
Slack Channel: #epidemic-model
Trello Board: https://trello.com/b/IfCsHxzg/epidemic-model
Github Code: TBC

3. When do you expect to make an impact with your work stream and what do you need to make it happen now?

ASAP. Focus.

a) Build and deploy user-facing application. Move fast, don't worry about breakages at this point. I like the idea of https://www.streamlit.io/ that @Felix Wittmann had: https://projectathenaco.slack.com/archives/C010HB92S00/p1585924676264700. Not very scalable, but quick to get up and running.
b) Develop and deploy simple corona model for all countries.

After that there is lots to do:
- Add deaths
- Iterate on the model
- Add maps
- Predict down to a region level
- See if we can ascertain the true level of infection (those that have not been tested)
- Better infrastructure deployment
- Swap out streamlit for something more scalable/web-friendly
- Build out API's to access predictions, so that others can use the models
- Predict virus phases in each country/region. Present "panic map".
- [Can we model the hidden number of infected people region by region and thereby determine the actual effect of testing and interventions](https://projectathenaco.slack.com/archives/C010HB92S00/p1585229987023700?thread_ts=1584951203.020700&cid=C010HB92S00)
- Comparison to SARS
- [VS. hospital beds](https://en.m.wikipedia.org/wiki/List_of_countries_by_hospital_beds)

4. What are impediments that you are facing?

Misunderstandings in direction. No public facing applications. I hope to solve that with this.

5. Immediate next steps

- Finish off simple bayesian modeling.
- Find someone to start building front-end @Felix Wittmann?


## Tasks

In lieu of access to the Trello board, here are some initial user stories.

- As a user I want public access to dashboard to use the epidemic team's models so that I can view predictions.
- As a data scientist, I want to be able to update the models hosted in the public environment.
- As an engineer, I need a dashboard solution quickly, so that I can deliver something to the users.

To achieve these, I propose three major strands of work:
1) Infrastructure - Where is this hosted? How is it deployed?
2) Application - What is running? How do we add models/dashboards?
3) Data Science - Work on the models, see below.

Note that we're not yet interested in perfection. The model(s) do(es)n't have to be perfect. In fact, they barely have to do anything at this point. We can improve them as we go. But the first priority is to get something working.

So I need at least one, at most three other people to help me figure this out. These are my initial proposals.

1) Infrastructure
- Ask Alex about hosting infrastructure. Best case we have some cloud infra somewhere. Worst case I'll suck up the cost temporarily on a simple k8s cluster.
- Create a repo and CI to deploy said infra. (Personally prefer gitlab + GCP, I'm happy to host this.)
- Create and maintain credentials to access cluster.
Notes: Quick wins. Don't worry about dev/prod separation at this stage. We can figure it out later.

Deliverables: Be able to deploy containers to the cluster and publicly expose them.

2) Application
- Create a repo and CI to use k8s credentials to deploy app. (Gitlab preferred, I can host this.)
- Create initial https://www.streamlit.io/ application.
- CI Pipeline to build container and deploy.
- Deploy initial application to k8s.
- Publicly expose app
- Document how people can update/add to the application

Deliverables: Initial dummy application. Public access. Instructions how to update.

3) Data Science
Phase 1:
- Implement a dummy model in streamlit and deploy to cluster
- Implement a dummy dashboard

Deliverables: Some kind of dummy model, public facing.

Phase 2:
- Implement a pipeline to retrain the model daily

Deliverables: Model is automatically retrained and model parameters are updated. App is automatically updated.

Phase 3:
- Implement a more advanced model (still simple though!). Logistic or SIR or something. Both seem to generate reasonable results.
- Update production.

Deliverables: Better model, public, retrained daily.

**Future**

We can come up with more priorities once the application is up and running. Most of this work will be data science-related, but there is scope for application improvements (e.g. better dashboards, more information) and infra improvements (staging environment, improve everyone's CI/CD). On the DS side, see the previous post for more info. I will add more later.


This repository contains the following notebooks:

- Analysis
    - [Covid-19 data analysis](./covid_data_analysis.ipynb)
- Bayesian Models
    - [Covid-19 exponential model](./covid_exponential_model.ipynb)
    - [Covid-19 exponential model with simple backtesting](./covid_exponential_model_backtest.ipynb)
